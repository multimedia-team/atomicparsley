Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: AtomicParsley
Upstream-Contact: https://bitbucket.org/wez/atomicparsley/issues
Source: https://bitbucket.org/wez/atomicparsley
 hg::https://bitbucket.org/wez/atomicparsley

Files: *
Copyright: 2005-2007, puck_lock@users.sourceforge.net
  2009, 2011, Santino Fuentes <santinofuentes@hotmail.com>
  2009, Josh Aune <luken@omner.org>
  2009-2012, Wez Furlong <wez@netevil.org>
  2010, Edriss Mirzadeh
  2010-2012, Oleg Oshmyan <chortos@inbox.lv>
  2011, John D Pell <john+BitBucket+AP@gaelicWizard.net>
  2014, Paul Foose <paul.foose+TourtiseHgRepo@gmail.com>
License-Grant:
 AtomicParsley is GPL software;
 you can freely distribute, redistribute, modify & use
 under the terms of the GNU General Public License;
 either version 2 or its successor.
License: GPL-2+

Files: src/extras/*
 src/ap_types.h
 src/sha1.cpp
Copyright: 1987-1998,2000-2001,2003-2005, Free Software Foundation, Inc.
License-Grant:
 This program is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
License: GPL-2+

Files: src/iconv.cpp
Copyright: 1998-2003, Daniel Veillard
  2005-2007, puck_lock@users.sourceforge.net
License-Grant:
 AtomicParsley is GPL software;
 you can freely distribute, redistribute, modify & use
 under the terms of the GNU General Public License;
 either version 2 or its successor.
License: GPL-2+ and Expat with Veillard Exception
 Except as contained in this notice,
 the name of Daniel Veillard shall not be used
 in advertising or otherwise
 to promote the sale, use or other dealings in this Software
 without prior written authorization from him.

Files: src/uuid.cpp
Copyright: 1989, Digital Equipment Corporation, Maynard, Mass.
  1989, Hewlett-Packard Company, Palo Alto, Ca.
  1998, Microsoft
  1990-1993, 1996, Open Software Foundation, Inc
  2006-2007, puck_lock@users.sourceforge.net
License-Grant:
 AtomicParsley is GPL software;
 you can freely distribute, redistribute, modify & use
 under the terms of the GNU General Public License;
 either version 2 or its successor.
License: GPL-2+ and NTP~uuid

Files: debian/*
License-Grant:
 This program is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation;
 either version 3, or (at your option) any later version.
Copyright: 2009-2010,2012-2013,2015-2019, Jonas Smedegaard <dr@jones.dk>
License: GPL-3+

License: GPL-2+
License-Reference: /usr/share/common-licenses/GPL-2

License: GPL-3+
License-Reference: /usr/share/common-licenses/GPL-3

License: Expat
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files
 (the "Software"),
 to deal in the Software without restriction,
 including without limitation
 the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice
 shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO
 THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: NTP~uuid
 To anyone who acknowledges that this file is provided "AS IS"
 without any express or implied warranty:
 permission to use, copy, modify, and distribute this file
 for any purpose is hereby granted without fee,
 provided that the above copyright notices and this notice
 appears in all source code copies,
 and that none of the names
 of Open Software Foundation, Inc., Hewlett-Packard Company,
 Microsoft, or Digital Equipment Corporation
 be used in advertising or publicity
 pertaining to distribution of the software
 without specific, written prior permission.
 Neither Open Software Foundation, Inc., Hewlett-Packard Company,
 Microsoft, nor Digital Equipment Corporation
 makes any representations about the suitability of this software
 for any purpose.
